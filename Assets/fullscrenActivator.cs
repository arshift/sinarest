﻿using UnityEngine;
using System.Collections;

public class fullscrenActivator : MonoBehaviour {

    PlayVideo video;
    VideoPlaybackBehaviour behaviour;
	// Use this for initialization
	void Start () {
        video = FindObjectOfType(typeof(PlayVideo)) as PlayVideo;
	}
	
    bool Toggle()
    {
        return video.playFullscreen = !video.playFullscreen;
    }
	public void TogglePlayFull()
    {
        behaviour.VideoPlayer.Play(Toggle(), behaviour.VideoPlayer.GetCurrentPosition());
        
    }
    private void Update()
    {
        if (!behaviour) behaviour = FindObjectOfType<VideoPlaybackBehaviour>();
    }
}
